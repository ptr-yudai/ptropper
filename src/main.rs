extern crate clap;
extern crate keystone;

use clap::{App, Arg};
mod binary;
mod converter;
mod gadget;
mod util;

/*
 * Entry Point
 */
fn main() {
    /* Define arguments */
    let app = App::new("ptropper")
        .version("0.1.0")
        .author("ptr-yudai <ptr.yudai@gmail.com>")
        .about("Interactive ROP gadget finder")
        .arg(Arg::with_name("filepath")    // --flag -f
             .short("f").long("file")
             .help("Target binary")
             .takes_value(true)
             .required(true));

    /* Parse arguments */
    let matches = app.get_matches();

    /* Open target binary file */
    let bin: binary::Binary;
    if let Some(filepath) = matches.value_of("filepath") {
        bin = binary::Binary::new(filepath).unwrap();
    } else {
        panic!("filepath is not initialized");
    }

    /* Create engine */
    let engine = converter::Converter::new("x86", "mode64", "little");

    /* Find gadget */
    gadget::find_by_instr(engine, bin,
                          vec!["pop rdi".to_string()],
                          "ret".to_string(), None, None);
}
