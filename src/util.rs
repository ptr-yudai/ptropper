pub fn find_bytes(haystack: &Vec<u8>, needle: &Vec<u8>) -> Option<usize>
{
    for i in 0..haystack.len() - needle.len() + 1 {
        if haystack[i .. needle.len() + i] == needle[..] {
            return Some(i);
        }
    }
    None
}

pub fn find_bytes_reverse(haystack: &Vec<u8>, needle: &Vec<u8>) -> Option<usize>
{
    let mut rev_needle = needle.clone();
    rev_needle.reverse();

    for i in (0..haystack.len() - needle.len() + 1).rev() {
        if haystack[i .. needle.len() + i] == rev_needle[..] {
            return Some(i);
        }
    }
    None
}
