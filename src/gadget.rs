use crate::binary::Binary;
use crate::converter::Converter;
use crate::util::find_bytes;

/// Finds ROP gadgets by instruction
///
/// # Arguments
///
/// * `engine` - Assembler / Disassembler engine
/// * `bin` - Binary object to find the gadgets from
/// * `body` - A gadget needs to have all of these instructions
/// * `tail` - A gadget needs to end with this instruction
/// * `head` - A gadget needs to start with this instruction (Optional)
/// * `depth` - Maximum number of instructions in a gadget (Default=6)
pub fn find_by_instr(
    engine: Converter,
    bin: Binary,
    _body: Vec<String>,
    tail: String,
    head: Option<String>,
    depth: Option<u32>
)
{
    // Default max-depth is 6
    let _max_depth = depth.unwrap_or(6);

    // [FIX] This value should be decided based on the architecture
    // Maximum length per instruction
    let _max_instr_len = 15;

    let asm_tail = engine.asm(tail, 0);

    for section in bin.sections.iter() {
        // Search gadgets for each section
        let mut offset = 0;

        // Check the gadget while the tail instruction is included
        while let Some(pos) = find_bytes(&section.bytes[offset..].to_vec(), &asm_tail.bytes) {
            let _old_offset = offset;
            offset += pos + asm_tail.bytes.len();

            // Extract a tiny frame to find the gadget from
            if head.is_some() {
                // Find the beginning
                //while let Some(delta) = find_bytes_reverse(&section.bytes[old_offset..].to_vec(), &head.unwrap().bytes) {
                //    
                //}
            } else {
                // 
                
            }
        }
        println!("{}", offset);
    }
}
