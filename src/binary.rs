extern crate elf;

use std::fs::File;
use std::io::{Error, Read};

pub struct Section {
    pub address: u64,
    pub bytes: Vec<u8>,
}

pub struct Binary {
    pub file: File,
    pub sections: Vec<Section>,
}

impl Binary {
    pub fn new(filepath: &str) -> Result<Binary, Error> {
        let mut file = match File::open(filepath) {
            Err(why) => panic!("Cannot open {}: {}", filepath, why),
            Ok(file) => file,
        };

        let mut sections: Vec<Section> = Vec::new();
        let mut buffer: Vec<u8> = Vec::new();
        file.read_to_end(&mut buffer)?;
        println!("{}", buffer.len());
        sections.push(Section { address: 0, bytes: buffer });

        Ok(Binary {file: file, sections: sections})
    }
}
