extern crate capstone;
extern crate keystone;

pub struct Converter {
    cs: capstone::Capstone,
    ks: keystone::Keystone
}

impl Converter {
    pub fn new(
        arch: &str,
        mode: &str,
        endian: &str
    ) -> Converter
    {
        let (cs_arch, ks_arch) = match arch {
            "x86"   => (capstone::Arch::X86, keystone::Arch::X86),
            _ => panic!("{}: No such architecture", arch)
        };
        let (cs_mode, ks_mode) = match mode {
            "mode64" => (capstone::Mode::Mode64, keystone::keystone_const::MODE_64),
            _ => panic!("{}: No such mode", mode)
        };
        let (cs_endian, ks_endian) = match endian {
            "little" => (capstone::Endian::Little, keystone::keystone_const::MODE_LITTLE_ENDIAN),
            _ => panic!("{}: No such endian", endian)
        };

        let cs = capstone::Capstone::new_raw(cs_arch, cs_mode, capstone::NO_EXTRA_MODE, Some(cs_endian))
            .expect("Cannot initialize Capstone engine");
        let ks = keystone::Keystone::new(ks_arch, ks_mode | ks_endian)
            .expect("Cannot initialize Keystone engine");
        ks.option(keystone::OptionType::SYNTAX, keystone::OPT_SYNTAX_NASM)
            .expect("Cannot set option to nasm syntax (keystone)");

        Converter { cs: cs, ks: ks }
    }

    pub fn asm(self: &Converter, code: String, address: u64)
               -> keystone::AsmResult {
        self.ks.asm(code, address).unwrap()
    }

    pub fn disasm(self: &Converter, code: &[u8], address: u64)
                  -> capstone::Instructions {
        self.cs.disasm_all(code, address).unwrap()
    }
}
